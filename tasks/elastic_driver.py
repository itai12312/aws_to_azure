import requests
import time
import elastic_utilities
import pickle
import json
HTTP_REQUEST_TIMEOUT = 1200000
API_BASE_URL = elastic_utilities.getAPIBaseURL('development')
API_KEY = elastic_utilities.getSDKAPIKeyAddUser()
#logging.basicConfig(filename=['data']['logs_dir']+"process_image.log",level=logging.DEBUG)

def addOwner(newOwnerContainer):
    URL = API_BASE_URL + 'v1/authentication/signUpAutomationTest';
    options = {'uri': URL,'method': 'POST','headers': newOwnerContainer['headers'],'json': newOwnerContainer['owner'],'timeout': HTTP_REQUEST_TIMEOUT};
    try:
        #add_owner_res = requests.post(options); # find correct request format
        add_owner_res = requests.post(URL, headers=newOwnerContainer['headers'], json=newOwnerContainer['owner'], timeout=options['timeout'])
        time.sleep(3);
        return newOwnerContainer['owner']
    except Exception as err:
        #var validation_obj = logger.responseValidation(null, err, "addOwner");
        #return validation_obj;
        print "failed adding owner:",err, add_owner_res.text
        return None

def loginOwner(owner, addapikey, cb):
    URL = API_BASE_URL + 'v1/authentication/login';
    options = {'uri': URL,'method': 'POST','headers': {'content-type' : 'application/json', 'api_key': elastic_utilities.getAutomationToken()},'json': owner,'timeout': HTTP_REQUEST_TIMEOUT};
    if not addapikey:
        options = {'uri': URL,'method': 'POST','headers': {'content-type' : 'application/json'},'json': owner,'timeout': HTTP_REQUEST_TIMEOUT}
    try:
        obj = requests.post(URL, headers=options['headers'], json=options['json'], timeout=options['timeout'])
        time.sleep(1)
        owner['token'] = json.loads(obj.text)['token']
        #logger.logToConsole('request executed successfully : login owner.');
        return owner
    except Exception as err:
        #var validation_obj = logger.responseValidation(null, err, "loginOwner");
        print "failed login owner:",err,obj.text
        return None

def addApp(newAppContainer, owner, cb):
    newApp = newAppContainer['app'];
    URL = API_BASE_URL + 'v1/apps/add-app-automation-test';
    newApp['ownerID'] = '392c908e-2968-45b3-91bb-8722fba344ee' # owner['ownerID'];
    newAppContainer['headers']['api_key'] = owner['token'];
    options = {'uri': URL,'method': 'POST','headers':  newAppContainer['headers'],'json': newApp,'timeout': HTTP_REQUEST_TIMEOUT};
    try:
        obj = requests.post(URL, headers=options['headers'], json=options['json'], timeout=options['timeout'])
        time.sleep(1);
        newApp['apiKey'] = json.loads(obj.text)['apiKey']
        newApp['appID'] = json.loads(obj.text)['appID']
        #logger.logToConsole('request executed successfully : add app.');
        return newApp
    except Exception as err:
        #var validation_obj = logger.responseValidation(null, err, "addApp");
        print "failed adding app",err,obj.text
        return None;

def getTokenAutomationTest(userData):
    URL = API_BASE_URL + 'v1/authentication/get-token'
    rejectUnauthorized = False
    options = {'uri': URL,'method': 'POST','headers': {'content-type': 'application/json'},'json': userData,'timeout': HTTP_REQUEST_TIMEOUT};
    try:
        obj = requests.post(URL, headers=options['headers'], json=options['json'], timeout=options['timeout'])
        time.sleep(1)
        #logger.logToConsole('request executed successfully : get token.');
        return obj;
    except Exception as err:
        #var validation_obj= logger.responseValidation(null, err, "getTokenAutomationTest");
        print "failed auth:",err,obj.text
        return None;

def addUserNew(user):
    URL = API_BASE_URL + 'v1/users';
    rejectUnauthorized = False;
    options = {
        'uri': URL,
        'method': 'POST',
        'headers': {
            'content-type' : 'application/json',
            'api_key' : user['api_key'] ,
            'device_id': user['deviceID']
        },
        'json': user,
        'timeout': HTTP_REQUEST_TIMEOUT
    }
    try:
        obj = requests.post(URL, headers=options['headers'], json=options['json'], timeout=options['timeout'])
        #time.sleep(1);
        #logger.logToConsole('request executed successfully : add user.');
        return obj;
    except Exception as err:
        #var validation_obj= logger.responseValidation(null, err, "addUser");
        print "failed addind user:",err, obj.text
        return None;

def addBucketnew(user,imagesContainer, cb):
    URL = API_BASE_URL + 'v1/images/add-bucket';
    rejectUnauthorized = False;
    options = {'uri': URL,'method': 'POST','headers': imagesContainer['headers'],'json': imagesContainer['body'],'timeout': HTTP_REQUEST_TIMEOUT};
    try:
        obj = requests.post(URL, headers=options['headers'], json=options['json'], timeout=options['timeout'])
        time.sleep(1)
        #logger.logToConsole('request executed successfully : add bucket.');
        return obj;
    except Exception as err:
        print "failed adding image:",err,obj.text
        #var validation_obj= logger.responseValidation(null, err, "addBucket");
        return None;

def createAppAndOwner(ownerName, email, password, appname, savepath):
    new_owner = elastic_utilities.createOwner(ownerName, email, password)
    add_owner_res = addOwner(new_owner)
    if not add_owner_res:
        return False
    owner_login_result = loginOwner(add_owner_res, None,True)
    if not owner_login_result:
        return False
    new_app = elastic_utilities.createApp(appname)
    add_app_result = addApp(new_app, owner_login_result, True)
    if not add_app_result:
        return False
    with open(savepath, 'w+') as f:
        json.dump({'new_app':new_app,'new_owner':new_owner}, f, protocol=pickle. HIGHEST_PROTOCOL)


def mainUseCase_new(imagename, facesFeatures, globalFeatures, root_uncompressed, appid, apiKey, coremodel):
    userid = imagename.split("/")[0]
    newUser = elastic_utilities.createUser(userid, appid, apiKey)
    get_token_result = getTokenAutomationTest(newUser)
    if not get_token_result:
        return False
    newUser['api_key'] = json.loads(get_token_result.text)['token'] # changes from time to time eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJRCI6IjQ5MGNkNzg3LTg2YjEtNDc2Yi05YjM5LTk3ZDI2MDI0YWRjMSIsImRldmljZUlEIjoiMDA2OGQ0OGEtYzI0NC00Mzg4LTgxNWEtNzI3ZGVmYWRjMTI0Iiwicm9sZSI6InNkay1jdXN0b21lciIsImtleVZlcnNpb24iOiJ2MiIsImlhdCI6MTUyODYxMzE4OSwiZXhwIjoxNTQ0MTY1MTg5fQ.875ND5YjjBikIzUeSHwvVGOUSB6OLoIRbdcROTqldxQ
    add_user_res = addUserNew(newUser)
    if not add_user_res:
        return False
    image = elastic_utilities.createImages(imagename, newUser['api_key'], facesFeatures, globalFeatures, root_uncompressed, coremodel)
    res_add_bucket = addBucketnew(None, image, None)
    if not res_add_bucket or res_add_bucket.status_code != 200:
        print "error is:", res_add_bucket.text
        return False
    return True



"""
        getBestCampaign: async function(user) {
            var reqBody = {
                "adUnitIDs": ["146351541"],
                "appID": user.appID,
                "deviceID": user.deviceID
            }
            const URL = API_BASE_URL + 'v1/campaigns/best-campaigns';
            var options = {uri: URL,method: 'POST',headers: {'content-type': 'application/json', 'api_key': user.api_key, 'device_id': user.deviceID},json: reqBody,timeout: HTTP_REQUEST_TIMEOUT};
            try {
                var obj = await request(options);
                logger.logToConsole('request executed successfully : best campaingn.');
            }
            catch (err) {
            }
        },

        userDeleteAllData: async function(user) {
            logger.logToConsole('userDeleteAllData ...');
            const URL = API_BASE_URL + 'v1/user-delete-all-data';
            var options = {uri: URL,method: 'DELETE',headers: {'content-type': 'application/json', 'api_key': user.api_key, 'device_id': user.deviceID},timeout: HTTP_REQUEST_TIMEOUT};
            try {
                var obj = await request(options);
                logger.logToConsole('request executed successfully : best campaingn.');
            }
            catch (err) {
            }
        }

    get_stats : async function (token) {
        const URL = API_BASE_URL + 'v1/demo/stats-by-device-id-4/ed48c14d-b355-41f0-aa5d-64b6ce636a60/pix_demo';
        var options = {uri: URL,method: 'GET',headers: {'content-type' : 'application/json', 'api_key': token},timeout: HTTP_REQUEST_TIMEOUT};
        try{
            var obj = await request(options);
            logger.logToConsole('request executed successfully : get stats.');
            return obj;
        }
        except Exception as err:

        }
        // request(options, function (err, response,body ) {
        //     var validation_obj = logger.responseValidation(response, err, "loginOwner")
        //     if(!validation_obj.is_valid){
        //         return cb(validation_obj, owner);
        //     }
        //     owner.token = body.token;
        //     logger.logToConsole('request executed successfully : login owner.');
        //     sleep.sleep(3);
        //     cb(validation_obj, owner)
        //
        // });
    },
"""
