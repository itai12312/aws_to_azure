import subprocess, random, os

def gen_command(command, *args):
    return command.format(*args)

def exec_command(command):
    status = True
    print command
    try:
        out = subprocess.check_output(command, shell=True)
        print "output:", out
    except Exception as e:
        print e.message
        status = False
    return status

def check_output(command):
    status = True
    print command
    try:
        return subprocess.check_output(command, shell=True)
    except Exception as e:
        print e.message
        status = False
    return status

def yield_aws(path, option, source_aws, root, logs_dir):
    #if not source_aws:
    full_path = os.path.join(root, path)
    for file in os.listdir(full_path):
        current_full = os.path.join(full_path, file)
        current = os.path.join(path, file)
        if option == 'folders' and os.path.isdir(current_full):
            if current[-1] != "/":
                current += "/"
            yield current
        elif option == 'files' and not os.path.isdir(current_full):
            yield current
    return

    # if len(path.split("/")) > 2:
    #     path_i = path.split("/")[-2].replace("'", "").replace("/", "+")
    # else:
    #     path_i = path[:-10].replace("'", "").replace("/", "+")
    #
    # report_path = "{}{}{}{}".format(logs_dir, path_i, "_folders.txt", random.randrange(1, 10000000, 1))
    # #print "adding_users", report_path
    #
    # exec_command(gen_command('touch \"{0}\"', report_path))
    # exec_command(gen_command('aws s3 ls \"s3://photos-extractor/{0}\" > \"{1}\"',path, report_path))
    #
    # folder_distinguisher = "                         PRE "
    # with open(report_path, 'r') as f:
    #     for line in f:
    #         line = line.replace("\n", "")
    #         if option == 'folders' and folder_distinguisher in line and line != "\n":
    #             line = line.split(folder_distinguisher)[-1]
    #             print "adding folder", path+line
    #             yield path+line
    #         if option == 'files' and not folder_distinguisher in line and line != "\n":
    #             spl = line.split(" ")
    #             #print "spl", spl
    #             if len(spl) > 2:
    #                 i = 2
    #                 for i in range(2, len(spl)):
    #                     if spl[i] != '':
    #                         break
    #                 line = " ".join(spl[i+1:])
    #                 print "adding file", path+line
    #                 yield path+line
    # #print "finished_adding_users"
    # exec_command(gen_command('rm -f \"{0}\"', report_path))

