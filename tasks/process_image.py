from _base import BaseTask
import os, sys, subprocess
#import dal.dal
import dal.db
import hashlib
import random
import numpy as np
from PIL import Image
from time import gmtime, strftime
import aws_utilities
from collections import defaultdict
import elastic_driver
import vision
import logging


class ImageProcessTask(BaseTask):

    def __init__(self, config, in_queue=None, out_queue=None, db=None):
        super(ImageProcessTask, self).__init__(in_queue, out_queue)
        for att in ['root_compressed','root_uncompressed']:
            assert config['data'][att][-1] == config['data'][att][0] and config['data'][att][-1] == "/"
        self.root_uncompressed = config['data']['root_uncompressed']
        self.root_compressed = config['data']['root_compressed']
        self.db = db
        self.core_path = config['core']['core_path']
        self.model_path = config['core']['model_path']
        self.logs_dir = config['data']['logs_dir']
        self.coremodel = config['core']['core_model']
        self.appid = config['apps']['appid']
        self.apiKey = config['apps']['apiKey']
        #logging.basicConfig(filename=self.logs_dir+"process_image.log",level=logging.DEBUG)

    def db_image_creation_and_metadata(self, image):
        orig_path = "{0}{1}".format(self.root_uncompressed, image)
        dest_path = "{0}{1}".format(self.root_compressed, image)
        orig_exists, dest_exists = False, False
        if os.path.exists(orig_path):
            orig_exists = True
        if os.path.exists(dest_path):
            dest_exists = True

        image_object = self._create_image_object(orig_exists, dest_exists, orig_path, dest_path, image, True)
        self.db.write_images([image_object])
        logging.debug("finished task for image:{0}".format(image))

    def get_att(self, array, att):
        if att in array:
            return array[att]
        return []

    def task(self, image):
        logging.debug("adding image {0}\n".format(image))
        orig_exists, dest_exists, orig_path, dest_path, compressed_ok = self._compress_image(image)
        elastic = self.db.read_images([self._calc_image_id(image)])
        if len(elastic) == 0: # create image in db
            image_object = self._create_image_object(orig_exists, dest_exists, orig_path, dest_path, image, compressed_ok)
            self.db.write_images([image_object])
            logging.debug("writing db")
            elastic = [{'_source':image_object}]
        field = 'signals'
        elastic = elastic[0]['_source']
        override = False
        condition = (override or not field in elastic or self.coremodel not in elastic[field]) and dest_exists
        logging.debug("condition [{0}], [{1}],[{2}],[{3}] ".format(condition, not field in elastic, self.coremodel not in elastic[field] if field in elastic else True,dest_exists))
        if condition: # send signals to elastic and api
            values = self.gen_signals(image, self.root_compressed)
            globs = self.get_att(values, '_global.npy')
            logging.debug("generating signals values, len of globs is:{0}".format(len(globs)))
            if len(globs) > 0:
                status = elastic_driver.mainUseCase_new(image, self.get_att(values, '_probs_face_out.npy'),globs, self.root_uncompressed, self.appid, self.apiKey, self.coremodel)
                logging.debug("image status: values=[{0}]".format(values))
                signals = elastic[field] if field in elastic else {}
                values_keys = values.keys()
                for att in values_keys:
                    if len(values[att]) == 0:
                        del values[att]
                    else:
                        values[att] = values[att].tolist()
                glob = '_global.npy'
                if status and glob in values and len(values[glob]) > 0:
                    logging.debug("gloabl for image:[{0}] globals=[{1}]".format(image ,values[glob]))
                    signals[self.coremodel] = values # use .toList to convert to numpy
                    self.db.update_images(self._calc_image_id(image),{'signals':signals, 'signals_core_'+self.coremodel:True})
        """
        field = 'azure_vision'
        if dest_exists and field not in elastic:
            values = vision.annotate_vision_url(dest_path, 0, 'path', 'vision')
            if len(values) > 0:
                self.db.update_images(self._calc_image_id(image),{field:values})
        """
        logging.debug("finished task for image:{0}".format(image))

    def _copy(self, imagepath_in_aws):
        location = "{0}{1}".format('/'.join(imagepath_in_aws.split("/")[:-1]), "/")
        aws_utilities.exec_command(aws_utilities.gen_command("aws s3 cp \"s3://photos-extractor/{}\" \"{}{}\"", imagepath_in_aws, self.root_uncompressed,location))

    def _create_image_object(self, orig_exists, dest_exists, orig_path, dest_path, imagepath_in_aws, compressed_ok):
        image_object = {}
        image_object['imageid'] = self._calc_image_id(imagepath_in_aws)
        image_object['imagepath'] = imagepath_in_aws
        image_object['type'] = imagepath_in_aws.split(".")[-1]
        image_object['user'] = imagepath_in_aws.split("/")[0]
        image_object['folder'] = imagepath_in_aws.split("/")[-2] if len(imagepath_in_aws.split("/")) > 1 else ''
        image_object['compressed_ok'] = compressed_ok

        # os.stat('somefile.ext').st_size
        for idx, bool in enumerate([orig_exists, dest_exists]):
            image_type = ['uncompressed', 'compressed'][idx]
            current_path = [orig_path, dest_path][idx]
            if bool:
                image_object['exists_{0}'.format(image_type)] = True
                image_object['size_on_{0}'.format(image_type)] = os.stat(current_path).st_size
                try:
                    width, height = Image.open(imagepath_in_aws).size
                    image_object['valid_{0}'.format(image_type)] = True
                    image_object['width_{0}'.format(image_type)] = width
                    image_object['height_{0}'.format(image_type)] = height
                except Exception as e:
                    image_object['valid_{0}'.format(image_type)] = False
                    image_object['uncomprssed_eror_{0}'.format(image_type)] = e.message
            else:
                image_object['exists_{0}'.format(image_type)] = False

        image_object['date_taken'] = imagepath_in_aws.split("(")[-1].split(")")[0]
        image_object['date_added_to_db'] = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        return image_object

    def _calc_image_id(self, imagepath_in_aws):
        return hashlib.sha224(imagepath_in_aws).hexdigest()

    def _compress_image(self, imagepath_in_aws):
        imagepath, imagename = "{}{}".format('/'.join(imagepath_in_aws.split("/")[:-1]), "/"), imagepath_in_aws.split("/")[-1]
        orig_path = aws_utilities.gen_command("{0}{1}", self.root_uncompressed, imagepath_in_aws)
        dest_path = aws_utilities.gen_command("{0}{1}", self.root_compressed, imagepath_in_aws)

        command1 = aws_utilities.gen_command("mkdir -p \"{0}{1}\" && /usr/bin/convert \"{2}\" -auto-orient -resize '1000^>' -quality 70% -set filename:f '%i' \"{3}\"",self.root_compressed, imagepath, orig_path, dest_path)

        ok = True
        orig_exists, dest_exists = False, False
        if os.path.exists(orig_path):
            orig_exists = True
        if os.path.exists(dest_path):
            dest_exists = True

        if not dest_exists and orig_exists:
            ok = aws_utilities.exec_command(command1)
        if os.path.exists(dest_path):
            dest_exists = True
        return orig_exists, dest_exists, orig_path, dest_path, ok

    def clean_dir(self, zipname, unzippeddir, images_list, zips_list):
        aws_utilities.exec_command(aws_utilities.gen_command("rm -rf \"{0}\" \"{1}\" \"{2}\" \"{3}\"", zipname, unzippeddir, images_list, zips_list))

    def gen_signals(self, imagename, root):
        # /home/itai/projects/opencv/core/test/multilabel_merged/model/multilabel108.zip /home/itai/projects/listfiles.txt /home /home/itai/projects/zips.txt /home/itai/projects/logfile.txt 1
        images_list = aws_utilities.gen_command("{0}{1}",self.logs_dir,imagename.replace("/", "__"))
        zips_list = aws_utilities.gen_command("{0}{1}{2}",self.logs_dir,"zips_zips_",imagename.replace("/", "__"))
        logfile_core = '/dev/null'
        rando = "{0}".format(random.randrange(1, 100000000, 1))
        zipname = aws_utilities.gen_command('{0}zipped{1}{2}.zip', self.logs_dir,rando, imagename.replace("/", "__"))
        unzippeddir = aws_utilities.gen_command("{0}unzipped{1}{2}/",self.logs_dir,rando, imagename.replace("/", "__"))
        self.clean_dir(zipname, unzippeddir, images_list, zips_list)
        with open(images_list, 'w+') as f:
            f.write(imagename)
        with open(zips_list,'w+') as f:
            f.write(zipname)
        command = aws_utilities.gen_command("{0} \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\" \"{6}\"", self.core_path, self.model_path, images_list,root,zips_list, logfile_core, '1')
        aws_utilities.exec_command(command)
        aws_utilities.exec_command(aws_utilities.gen_command("unzip \"{0}\" -d \"{1}\"", zipname, unzippeddir))
        values = {}
        for att in ['_global.npy', '_multilabel.npy', '_detection.npy', 'face_landmarks_out.npy', '_probs_face_out.npy']:
            path = aws_utilities.gen_command("{0}{1}{2}", unzippeddir, imagename, att)
            if os.path.exists(path):
                arr = np.load(path)
                if len(arr) > 0:
                    values[att] = arr
        #self.clean_dir(zipname, unzippeddir, images_list, zips_list)
        #in main-elastic_driver.createAppAndOwner('itai', 'itai.zeitak@pixoneye.com', 'password', 'korean_data',
        #                                 self.credentials_path)
        return values





