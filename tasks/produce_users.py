import os, subprocess
from _base import BaseTask
import aws_utilities
import logging

class UserProduceTask(object):
    def __init__(self, config, in_queue=None, out_queue=None):
        self.in_queue = in_queue
        self.out_queue = out_queue
        self.root_uncompressed = config['data']['root_uncompressed']
        self.root_compressed = config['data']['root_compressed']
        self.source_aws = False
        self.logs_dir = config['data']['logs_dir']
        #logging.basicConfig(filename=self.logs_dir+"produce_users.log",level=logging.DEBUG)

    def run(self):
        out_items = self.list_dir()
        BaseTask.enqueque_items(self.out_queue, out_items)

    def list_dir(self):
        # root = self.root_uncompressed
        # print "adding_users"
        # assert root[-1] == "/"
        # command  = 'touch aws s3 ls s3://photos-extractor > sampl.txt'
        # for user in os.listdir(root):
        #     yield user + "/"
        #     print "adding user", user
        # print "finished_adding_users"
        logging.debug("starting:")
        #with open('/home/pixoneye/aws_to_azure/listusers.txt','r') as f:
        #    for line in f:
        #        yield line.replace("\n","").split(",")[0]+"/"
        dir = self.root_uncompressed
        folders = []
        for folder in aws_utilities.yield_aws('', 'folders', self.source_aws, dir, self.logs_dir):
            logging.debug(folder)
            folders.append(folder)
        return folders


