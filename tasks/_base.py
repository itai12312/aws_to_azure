from abc import ABCMeta, abstractmethod
import time

RETRIES = 15
SLEEP = 4

class BaseTask():
    __metaclass__ = ABCMeta

    def __init__(self, in_queue=None, out_queue=None):
        self.in_queue = in_queue
        self.out_queue = out_queue

    def run_tasks_from_queue(self):
        assert self.in_queue
        cont = True
        while cont:
            for tries in range(RETRIES):
                while not self.in_queue.empty():
                    item = self.in_queue.get()
                    self.run_task(item)
                #if not continueonempty and self.in_queue.empty():
                #    cont = False
                #    break
                time.sleep(SLEEP)

    @abstractmethod
    def task(self, item):
        pass

    @classmethod
    def enqueque_items(cls, queque, items):
        if queque is not None:
            for itm in items:
                queque.put(itm)

    def run_task(self, item):
        out_items = self.task(item)
        BaseTask.enqueque_items(self.out_queue, out_items)



