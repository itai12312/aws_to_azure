import os, json, requests

vision_keys= ["f6c7bb05093b4884aaf960f56ee8f5fb","2d59774140ce4878930f9450d2e29121","179d8fcdb66c429389cb1eb533191815",
                  "7f441828bea641949a02fa0568b9ffc2"]
vision_urls = ["https://northeurope.api.cognitive.microsoft.com/vision/v1.0/analyze",
               "https://eastus.api.cognitive.microsoft.com/vision/v1.0/analyze",
               "https://eastus.api.cognitive.microsoft.com/vision/v1.0/analyze",
               "https://eastus.api.cognitive.microsoft.com/vision/v1.0/analyze"]

def annotate_vision_url(image, servicenum, url_or_path, face_or_vision):
    key = vision_keys[servicenum]
    analyze_url = vision_urls[servicenum]

    assert (face_or_vision == 'vision' or face_or_vision == 'face') and (url_or_path == 'url' or url_or_path =='path')
    #if not os.path.exists(savepath):
    print("working on image", image)
    #Ocp-Apim-Subscription-Key
    headers = {'Ocp-Apim-Subscription-Key': key}
    if face_or_vision == 'vision':
        params = {'visualFeatures': 'Categories,Tags,Faces'} #Description
    else:
        params = {
            'returnFaceId': 'true',  # change back to true?
            'returnFaceLandmarks': 'false',
            'returnFaceAttributes': 'age,gender',
            # ,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise
        }

    if 'http://' in image or 'https://' in image or url_or_path == 'url':
        response = requests.post(analyze_url, headers=headers, params=params, json={'url': image})
    else:
        headers['Content-Type'] = "application/octet-stream"
        data = open(image, "rb").read()
        response = requests.post(analyze_url, headers=headers, params=params, data=data)
    try:
        response.raise_for_status()
    except Exception as ex:
        print(ex)
        return {}
    analysis = response.json()
    #with open(savepath, 'w+') as f:
    #    json.dump(analysis, f)
    return analysis

