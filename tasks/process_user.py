import os
from _base import BaseTask
import aws_utilities
import subprocess
import logging
class UserProcessTask(BaseTask):
    def __init__(self, config, in_queue=None, out_queue=None):
        super(UserProcessTask, self).__init__(in_queue, out_queue)
        for att in ['root_compressed','root_uncompressed']:
            assert config['data'][att].startswith("/") and config['data'][att].endswith("/")
        self.root_uncompressed = config['data']['root_uncompressed']
        self.root_compressed = config['data']['root_compressed']
        self.source_aws = False
        self.logs_dir = config['data']['logs_dir']
        #logging.basicConfig(filename=self.logs_dir+"process_users.log",level=logging.DEBUG)

    def task(self, item):
        return self._list_images(item)

    def _list_images(self, path):
        assert path.endswith("/")
        #logging.debug("in loop path=[{0}]".format(path))
        folders = [path]
        dir = self.root_uncompressed
        while len(folders) > 0:
            folder = folders.pop(0)
            for subfolder in aws_utilities.yield_aws(folder, 'folders', self.source_aws, dir, self.logs_dir):
                folders.append(subfolder)
            for image in aws_utilities.yield_aws(folder, 'files', self.source_aws, dir, self.logs_dir):
                yield image
            #logging.debug("finished with folder [{0}]".format(folder))
        #logging.debug("finished listing images for [{0}]".format(path))

            # for item in self._gen_images_from_folders(folders, folder):
            #    yield item
    # def _gen_images_from_folders(self, folders, folder):
    #     for subfolder in aws_utilities.yield_folders(folder):
    #         folders.append(subfolder)
    #     for image in aws_utilities.yield_files(folder):
    #         yield image

