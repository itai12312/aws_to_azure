import os
import sys
import subprocess
import time
import argparse
import testing
from multiprocessing import Process, Queue
from tasks import elastic_driver, elastic_utilities
from dal.elastic import ElasticDb
from tasks.process_image import ImageProcessTask
from tasks.process_user import UserProcessTask
from tasks.produce_users import UserProduceTask
from common import config
import logging


class TimedProcess:
    def __init__(self, p, t):
        self.p = p
        self.t = t

def get_simple_logger(logs, level='DEBUG'):
    #logging.basicConfig(filename=logs+"elastic.log",level=logging.DEBUG)
    level = getattr(logging, level.upper())
    root = logging.getLogger()
    root.setLevel(level)
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    root.addHandler(ch)
    return root


def spawn(task, number):
    timed_processes = []
    for i in range(number):
        timed_processes.append(
            TimedProcess(Process(target=task), time.time()))  # make sure comma after args is sending args
        timed_processes[i].deamon = True
        timed_processes[i].p.start()
    return timed_processes


def join(timed_processes):
    for tp in timed_processes:
        tp.p.join()  # Wait for the reader to finish
        print "Sending numbers to Queue() took %s seconds" % ((time.time() - tp.t))


def main(args):
    queue_users = Queue()
    queue_images = Queue()

    mode = args.mode
    configuration = config.get_config(mode)
    logger = get_simple_logger(configuration['data']['logs_dir'])
    db = ElasticDb(configuration['elastic'], logger)

    user_produce = UserProduceTask(configuration, in_queue=None, out_queue=queue_users)
    user_process = UserProcessTask(configuration, in_queue=queue_users, out_queue=queue_images)
    image_process = ImageProcessTask(configuration, in_queue=queue_images, out_queue=None, db=db)

    # elastic_driver.createAppAndOwner('itai', 'itai.zeitak@pixoneye.com', testing.password, 'korean_data',
    #                                 configuration['credentials_path'])
    #image_process.task('f24a3d0d-a09b-4747-b02d-c6e878f4088d/storage/9016-4EF8/DCIM/Camera/20180510/220343(2018-05-10T13-03-43Z).jpg')
    #image_process.task('0068d48a-c244-4388-815a-727defadc124/storage/emulated/0/NAVER Cloud/1500912585315s(2017-07-24T09-09-45Z).jpg')
    #image_process.run_task('0068d48a-c244-4388-815a-727defadc124/storage/emulated/0/SNOW/20180224_144524(2018-02-24T05-45-24Z).jpg')
    # self.db.update_images('4982f13e300e2191b9b279558dc8278ec065edc31fa2b6137901292c','sent_to_api_dev', status)
    # image_process.run_task('00445317-6af9-4325-8ccf-2a04583b6610/storage/emulated/0/Download/%C4%B6%EF%BF%BD%EF%BF%BD%EF%BF%BD%D7%B6%EF%BF%BD%EF%BF%BD%EF%BF%BD%EF%BF%BD%EF%BF%BD%C7%B0_001(2017-03-08T09-38-24Z).jpg')
    # return

    #user_produce.run()

    #user_process.run_tasks_from_queue()
    #image_process.run_tasks_from_queue()

    timed_processes = spawn(user_produce.run(), args.users_produce)
    timed_processes += spawn(user_process.run_tasks_from_queue, args.users_process)
    ##time.sleep(30)
    timed_processes += spawn(image_process.run_tasks_from_queue, args.image_process)
    join(timed_processes)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # nohup python main.py --users-process=10 --image-process=10  2>&1 > log.txt
    parser.add_argument("--users-produce", default=1, help="number of users-produce_workers", type=int)
    parser.add_argument("--users-process", default=1, help="number of users-process_workers", type=int)
    parser.add_argument("--image-process", default=1, help="number of image_processing_workers", type=int)
    parser.add_argument("--mode", default='local', help="mode", type=str)

    args = parser.parse_args()

    main(args)
