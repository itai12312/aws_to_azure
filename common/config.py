import os

apps = {
    'korean_data':
        {
            'appid':'490cd787-86b1-476b-9b39-97d26024adc1',
            'apiKey':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJRCI6IjQ5MGNkNzg3LTg2YjEtNDc2Yi05YjM5LTk3ZDI2MDI0YWRjMSIsInRva2VuUm9sZSI6InNkay1jdXN0b21lciIsInRva2VuVHlwZSI6InJlZnJlc2gtdG9rZW4iLCJrZXlWZXJzaW9uIjoidjIiLCJpYXQiOjE1Mjg2MTI5ODV9.9vCYF4yHzJOHj2NdxaRBfvEvphlaCRL_V0-lA-kP5EM'
        },
    'korean_data_dev':
        {
            'appid':'1b4eeceb-1057-4df5-8cec-a2b206164ba9',
            'apiKey':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJRCI6IjFiNGVlY2ViLTEwNTctNGRmNS04Y2VjLWEyYjIwNjE2NGJhOSIsInRva2VuUm9sZSI6InNkay1jdXN0b21lciIsInRva2VuVHlwZSI6InJlZnJlc2gtdG9rZW4iLCJrZXlWZXJzaW9uIjoidjIiLCJpYXQiOjE1Mjg3ODU5ODZ9.65-5ilplErnSLDieK__GjMcCsE27DlU8SSbdC4Wrcvs'
        }
}

core_settings = {
    'local':
        {
            'core_model':'4.0.1.b4fbfb57a4e03bbf5694b1007d0bc031',
            'core_path':'/home/itai/projects/opencv/core/cmake-build-debug/x64/bin/CoreRunner', #linux_core_build
            'model_path':'/home/itai/projects/opencv/core/test/multilabel_merged/model/multilabel108.zip'
        },
    'remote':
        {
            'core_model':'4.0.1.b4fbfb57a4e03bbf5694b1007d0bc031',
            'core_path':'/data/opencv/core/linux_core_build/x64/bin/CoreRunner', #linux_core_build
            'model_path':'/data/opencv/core/test/multilabel_merged/model/multilabel108.zip'
        }
}


data_settings = {
    'local':
        {
            'root_uncompressed':'/s3/',
            'root_compressed':'/data/',
            'logs_dir':'/home/itai/logs/',
            'logfile_core':'/home/itai/logs/',
        },
    'remote':
        {
            'root_uncompressed':'/s3/',
            'root_compressed':'/data/',
            'logs_dir':'/data/opencv/core/logs/',
            'logfile_core':'/data/opencv/core/logs/',
        }
}

elastic_settings = {
    'table_dev':
        {
            'port':9200,
            'hosts':'52.156.213.110',
            #"http://10.1.0.11:9200", "http://10.1.0.13:9200"]
            #'hosts': "52.156.213.110", # http://localhost:9200
            #http://52.178.208.125:5601/app/sense   http://10.1.0.100
            'images_index': 'full_gallery_images_demo',
            'doc_type':'images'
        },
    'table':
        {
            'port':9200,
            'hosts':'52.156.213.110',
            'images_index': 'full_gallery_images',
            'doc_type':'images'
        }
}


def get_apps(mode):
    return apps[mode]

def get_core_settings(mode):
    return core_settings[mode]

def get_elastic_settings(mode):
    return elastic_settings[mode]

def get_data(mode):
    return data_settings[mode]


config = \
    {
        'local':
            {
                'data':get_data('local'),
                'apps':get_apps('korean_data_dev'),
                'core':get_core_settings('local'),
                'elastic':get_elastic_settings('table_dev')
            },
        'remote':
            {
                'data':get_data('remote'),
                'apps':get_apps('korean_data'),
                'core':get_core_settings('remote'),
                'elastic':get_elastic_settings('table')
            },
        'remote_dev':
            {
                'data':get_data('remote'),
                'apps':get_apps('korean_data_dev'),
                'core':get_core_settings('remote'),
                'elastic':get_elastic_settings('table_dev')
            }
    }

def get_config(mode):
    return config[mode]

def get_conf():
    if os.path.exists("/home/itai"):
        return 'local'
    return 'remote'