import time, sys
from elasticsearch import Elasticsearch
from elasticsearch import helpers
import logging
# import simplelogger
#from common.decorators.logger_wrapper import exception_wrapper
#from dal.services.metrics_agent import statsd
import dictobj

class ElasticDb():
    def __init__(self, config, logger):
        self.es_client = ElasticsearchService(dictobj.DictionaryObject(config), logger)
        self.es_index = config['images_index']
        self.doc_type = config['doc_type']

    def read_images(self, images_ids):
        array = self.es_client.search(self.es_index, doc_type=self.doc_type, ids=images_ids)
        return array

    def update_images(self, imageid, values):
        array = self.read_images([imageid])
        print "array is", array
        if len(array) > 0:
            for fieldname in values:
                array[0]['_source'][unicode(fieldname)] = values[fieldname]
            self.write_images([array[0]['_source']])

    def read_all(self):
        array = self.es_client.search(self.es_index, doc_type=self.doc_type, ids=None)
        return array

    def write_images(self, images_documents):
        self.es_client.index_items(items=images_documents, index=self.es_index, doc_type=self.doc_type, id_mapping='imageid')

"""
    def make_image_doc(self, imageid, imagepath, type, user, folder, valid, date_taken, date_added_to_db,
                       size_on_disk, size_compressed_on_disk, width, height, uncompressed, compressed):

        return dict(imageid=imageid ,imagepath=imagepath, type=type, user=user, folder=folder, valid=valid, date_taken=date_taken,
                    date_added_to_db=date_added_to_db, size_on_disk=size_on_disk, size_compressed_on_disk=size_compressed_on_disk,
                    width=width,height=height, uncompressed=uncompressed, compressed=compressed)
"""


#@exception_wrapper
def _prepare_bulk_actions(items, index, doc_type, id_mapping, routing_key, type_mapping):
    for item in items:
        d_type = _get_doc_type(doc_type, item, type_mapping)
        doc = {
            "_index": index,
            "_type": d_type,
            "_source": item
        }

        if id_mapping:
            doc['_id'] = item[id_mapping]

        if routing_key:
            doc['_routing'] = item[routing_key]
        yield doc


#@exception_wrapper
def _get_doc_type(doc_type, item, type_mapping):
    d_type = doc_type
    if type_mapping:
        d_type = item.get(type_mapping, doc_type)
    return d_type


def _get_current_or_default(default, current=None):
    if current:
        return current
    else:
        return default


class ElasticsearchService(object):
    def __init__(self, config, logger):
        self._logger = logger
        self._es_client = Elasticsearch(hosts=config.hosts, port=config.port)
        self._retries = 10
        if "index_name" in config:
            self._index_name = config.index_name
        else:
            self._index_name = None
        if "doc_type" in config:
            self._doc_type = config.doc_type
        else:
            self._doc_type = None

    def index_items(self, items, index=None, doc_type=None, id_mapping=None, routing_key=None, doc_type_mapping=None):
        index = _get_current_or_default(self._index_name, index)
        doc_type = _get_current_or_default(self._doc_type, doc_type)
        tries = self._retries
        while tries:
            try:
                if tries < self._retries:
                    self._logger.warning("connecting DB from index. try: " + str(tries) + "index:" + index)
                #with statsd.timed('{env_job}.es.index.time'.format(env_job=self._logger.name),
                #                  tags=["index:{}".format(index)]):
                actions = _prepare_bulk_actions(items, index, doc_type, id_mapping, routing_key,
                                                doc_type_mapping)
                res = helpers.bulk(self._es_client, actions)
                #    statsd.increment(metric='{env_job}.es.index'.format(env_job=self._logger.name), value=1,
                #                 tags=["index:{}".format(index)])
                if res[1]:
                    self._logger.error("error had occurred while trying to save signals to es")
                    #statsd.increment(metric='{env_job}.es.index.error'.format(env_job=self._logger.name), value=1,
                    #                 tags=["index:{}".format(index)])
                    for error in res[1]:
                        self._logger.error(error)
                break
            except Exception as ex:
                self._logger.warning("ERROR CONNECTING DB for indexing - " + "index:" + index)
                self._logger.warning(ex)
                self._logger.warning("now should run for the next iteration - " + "index:" + index)
                # statsd.increment(metric='{env_job}.es.connect.error'.format(env_job=self._logger.name), value=1,
                #                  tags=["index:{}".format(index)])

                tries -= 1
        if not tries:
            self._logger.error("failed To Connect DB when on index_items!!!")
            # statsd.increment(metric='{env_job}.es.connect.error'.format(env_job=self._logger.name), value=1,
            #                  tags=["index:{}".format(index)])

    def search(self, index, doc_type=None, ids=None, extra_filters=None):
        index = _get_current_or_default(self._index_name, index)
        doc_type = _get_current_or_default(self._doc_type, doc_type)

        if ids is None:
            ids = []
        if not ids and not extra_filters:
            query = {"query": {"match_all": dict()}}
        else:
            query = {
                "query": {
                    "bool": {
                        "must": []
                    }
                }
            }
            if ids:
                query['query']['bool']['must'].append({"ids": {"values": ids}})
            if extra_filters:
                query['query']['bool']['must'] += extra_filters
        res = []
        tries = self._retries
        es_err = ""
        while tries:
            try:
                # with statsd.timed('{env_job}.es.scan.time'.format(env_job=self._logger.name),
                #                   tags=["index:{}".format(index)]):
                res = self._es_client.search(index= index,  doc_type=doc_type, body=query, request_timeout=30)
                if "hits" in res.keys():
                    if "hits" in res["hits"].keys():
                        return res["hits"]["hits"]
                    else:
                        return []
                else:
                    return []

            except Exception as e:
                #statsd.increment(metric='{env_job}.scan.error'.format(env_job=self._logger.name), value=1,
                #                 tags=["index:{}".format(index)])
                self._logger.warning("ERROR Searching - index:{}, err:{}m tries:{}".format(index, e, str(tries)))
                es_err = e
                tries -= 1
        if not tries:
            self._logger.warning("failed To Search DB!!!")
            raise es_err
        return res

    #@exception_wrapper
    def scan_items(self, index, doc_type=None, ids=None, extra_filters=None):
        index = _get_current_or_default(self._index_name, index)
        doc_type = _get_current_or_default(self._doc_type, doc_type)

        if ids is None:
            ids = []
        if not ids and not extra_filters:
            query = {"query": {"match_all": dict()}}
        else:
            query = {
                "query": {
                    "bool": {
                        "must": []
                    }
                }
            }
            if ids:
                query['query']['bool']['must'].append({"ids": {"values": ids}})
            if extra_filters:
                query['query']['bool']['must'] += extra_filters
        try:
            #with statsd.timed('{env_job}.es.scan.time'.format(env_job=self._logger.name),
            #                  tags=["index:{}".format(index)]):
            res = helpers.scan(self._es_client,  query=query, index=index, doc_type=doc_type)
        except Exception as e:
            #statsd.increment(metric='{env_job}.scan.error'.format(env_job=self._logger.name), value=1,
            #                 tags=["index:{}".format(index)])
            raise e
        return res

    #@exception_wrapper
    def scan_items_with_query(self, index, query):
        max_retries = 5
        for i in range(max_retries):
            try:
                #with statsd.timed('{env_job}.es.scan.query.time'.format(env_job=self._logger.name),
                #                  tags=["index:{}".format(index)]):
                items = helpers.scan(self._es_client,
                                     scroll='10m',
                                     index=index,
                                     request_timeout=None,
                                     query=query)

                return items
            except Exception as err:
                print "Scan helpers exception in scan_items_with_query"
                #statsd.increment(metric='{env_job}.es.scan.error'.format(env_job=self._logger.name), value=1,
                #                 tags=["index:{}".format(index)])

    @property
    def indices(self):
        #with statsd.timed('{env_job}.es.indices.time'.format(env_job=self._logger.name)):
        #    res = self._es_client.indices.get_alias().keys()
        #return res
        pass

    def get_indices_with_prefix(self, prefix):
        indices = [index for index in self.indices if index.startswith(prefix)]
        return indices
