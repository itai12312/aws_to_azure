import subprocess
from tasks import aws_utilities

class DBSQL():
    def write_images(self, image_objects):
        for image_object in image_objects:
            int_atts = []
            for att in ['size_on_compressed', 'size_on_uncompressed']:
                if att in image_object:
                    int_atts.append(att)

            keys = set(image_object.keys()) - set(int_atts)

            atts, values = ",".join(keys), "\",\"".join(str(image_object[v]) for v in keys)

            atts1, values1 = ",".join(int_atts), ",".join(str(image_object[v]) for v in int_atts)
            if len(int_atts) > 0:
                command = aws_utilities.gen_command(
                    "mysql images -e 'insert into images ({0},{1}) values (\"{2}\",{3}) on duplicate key update compressed_ok=VALUES(compressed_ok);'",
                    atts, atts1, values, values1)
            else:
                command = aws_utilities.gen_command(
                    "mysql images -e 'insert into images ({0}) values (\"{1}\") on duplicate key update compressed_ok=VALUES(compressed_ok);'",
                    atts, values)
            aws_utilities.exec_command(command)


    def update_image(self, image):
        command = "mysql images -u root -p12345 -e 'update images set downloaded=\"1\", compressed=\"1\" where imagepath=\"{}\";'".format(image)
        print command
        subprocess.check_output(command, shell=True)

    def wr_image(self, image):
        imagedate = image.split("(")[-1].split(".")[0]
        type = image.split(".")[-1]
        command = "mysql images -u root -p12345 -e 'insert into images (imagepath, type, date_image,downloaded,compressed) values(\"{}\", \"{}\", \"{}\",\"0\",\"0\") ON DUPLICATE KEY UPDATE downloaded=VALUES(downloaded);'".format(image, '', '')
        print command
        subprocess.check_output(command, shell=True)

